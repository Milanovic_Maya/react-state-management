import React from 'react';
import { Link } from 'react-router-dom';
import { Switch, Route  } from 'react-router-dom';
import logo from '../../logo.svg';
import './App.css';
import Counter from '../counter/Counter';
import Users from '../users/Users';
import { UserDetails } from '../users/UserDetails';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        {/* <Counter max={15} step={5} /> */}
        <Switch>
          <Route path={"/user/:id"} component={UserDetails} />
          <Route path={"/users"} component={Users}/>
          <Route path="/" component={() => {
            return (
              <div>
                <h1>
                  Home page
                </h1>
                <Link to="/users">See random user</Link>
              </div>);
          }}></Route>
        </Switch>

      </header>
    </div>
  );
}

export default App;
