import React from "react";
import { Link } from "react-router-dom";
// import { useFetch } from '../../hooks/customHooks';
// import { fetchUsers } from "../../actions/customActions";
// import { URL } from '../../constants';
import { useMainContext } from './../../context/mainContext';
const Users = props => {

  // const [response, loading, error] = useFetch(URL, fetchUsers);
  // const users = response;

  const { users: { userList, usersError} } = useMainContext();
    

  return (
    <div>
      {
        userList && userList.length ? (
          <div>
            {userList.map(user => (
              <div key={user.ip_address}>
                <p>
                {`User fetched: ${ user.first_name } ${ user.last_name }`}
                </p>
                <Link to={`/user/${ user.id }`}>Details</Link>
              </div>
            ))}
          </div>
        ) : "Loading ..."
      }
      {usersError && <p>Error: {usersError.message}</p>}
    </div>
  );
};

export default Users;