import React from "react";
import { Link } from 'react-router-dom';
import { useMainContext } from './../../context/mainContext';

export const UserDetails = props => {
  const { match: { params: { id } } } = props;
  // Read from context: 
  const { users: { userList } } = useMainContext();

  const userDetails = userList && userList.length ? userList.find(user => `${ user.id }` === id): null;

    return (
        <div>
        {userDetails ? (
          <div>
            <h1>Name: {`${ userDetails.first_name } ${ userDetails.last_name}`}</h1>
            <p>Email: {userDetails.email}</p>
            <p>Address: {userDetails.ip_address}</p>
          </div>
        ):null}
            <Link to="/">Back to Home page</Link>
            <br/>
            <Link to="/users">Back to Random user</Link>
        </div>
    )    
}