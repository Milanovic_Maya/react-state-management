import React from 'react';

import { useMainContext } from '../../context/mainContext';
import state from '../../state/state';


// const useLocalStorage = (initialState, key) => {
//     const get = () => {
//         const storage = localStorage.getItem(key);
//         if (storage) return JSON.parse(storage);
//         return initialState;
//     };
//     const [value, setValue] = React.useState(get());

//     React.useEffect(() => {
//         localStorage.setItem(key, JSON.stringify({ value }));
//     }, [value]);

//     return [value, setValue];
// };

const usePrevious = value => {
    const ref = React.useRef(); // will persist value between renders

    // will e called after the render with the prev value
    React.useEffect(() => { ref.current = value; });

    // this will provide the prev value of the ref (before setting the new value)
    return ref.current;
};

const Counter = ({ max, step }) => {

    // const [count, setCount] = React.useState(0);
    // const message = usePrevious(count);

    const { count: countContext, message } = useMainContext();

    // hooks- no callback and must return some state value
    const increment = () => countContext.incrCount();
    const decrement = () => countContext.decrCount();
    const reset = () => countContext.resetCount(state.initialState);


    console.log("message", message);

    // for side effects - everything else than setting state
    React.useEffect(() => {
        document.title = countContext.count;
        // return value like will unmount -remove subscriptions, etc
        // return ()=>clearInterval(id)


    }, [countContext.count]); // should these values change, do effect

    return (
        <div>
            {/* <p>prevCount:{message}</p> */}
            <p>{countContext.count}</p>
            <button onClick={increment}>+</button>
            <button onClick={reset}>SET to {state.initialState}</button>
            <button onClick={decrement}>-</button>
        </div>
    );
};

export default Counter;
