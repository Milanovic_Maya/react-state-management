import axios from "axios";
import { URL } from '../constants';

export const fetchUsers = dispatch => {
    axios.get(URL)
        .then(response => dispatch({ type: "RESPONSE_COMPLETE", response: response.data }))
        .catch(error => dispatch({ type: "ERROR", error }));
};