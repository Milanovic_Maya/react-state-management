import React from 'react';
// import axios from "axios";

// Thunk - for code that will be executed later

export const definitelyNotAThunk = () => {
    return function aThunk() {
        console.log("Hi, I'm thunk!");
    };
};

// dispatch an function instead of an actionobject
// for async calls
// when that function is done, it will return an object


// Abstraction for use reducer;
export const useThunkReducer = (reducer, initialState) => {
    const [state, dispatch] = React.useReducer(reducer, initialState);

    // Middleware with proxy:

    const dispatchProxy = React.useCallback(new Proxy(dispatch, {
        apply: (target, thisArg, args) => {
            const [action] = args;

            console.log("Calling proxy with action: ", action);

            if (typeof action === "function") {
                return action(target);
            } else {
                target(action);
            }
        }
    }), [dispatch]);

    // Middleware:
    const enhancedDispatch = React.useCallback(action => {
        console.log(action);
        if (typeof action === "function") {
            action(dispatch);
        } else {
            dispatch(action);
        }
    }, [dispatch]);

    return [state, dispatchProxy];
};
