import { initialState } from "../constants";

export const fetchReducer = (state = {}, action) => {
    if (action.type === "LOADING") {
        return initialState;
    }
    if (action.type === "ERROR") {
        return {
            loading: false,
            error: action.error,
            response: null
        };
    }
    if (action.type === "RESPONSE_COMPLETE") {
        return {
            response: action.response,
            error: null,
            loading: false
        };
    }
    return initialState;
};