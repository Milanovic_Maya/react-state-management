import React from 'react';
// import axios from "axios";
// import { URL } from "./constants";
import { useThunkReducer } from '../reducers/thunk';
import { fetchReducer } from '../reducers/customReducers';
import { initialState } from '../constants';



export const useFetch = (url, fetchData) => {
    // const [response, setResponse] = React.useState(null);
    // const [loading, setLoading] = React.useState(true);
    // const [error, setError] = React.useState(null);

    // const [state, dispatch] = React.useReducer(fetchReducer, initialState);
    const [state, dispatch] = useThunkReducer(fetchReducer, initialState);

    React.useEffect(() => {


        // Solution 1:
        // const httpGet = async () => {
        //     try {
        //         const response = await axios.get(URL);

        //         /** AXIOS .get vs method:get => response is the same!!! */

        //         // const response2 = await axios({
        //         //     method: 'get',
        //         //     url: URL
        //         // });

        //         // console.log("axios.get", response);
        //         // console.log("axios({method:'get'})", response2);


        //         setResponse(response.data.results);

        //         dispatch({ type: "RESPONSE_COMPLETE", response: response.data.results });
        //     } catch (error) {
        //         dispatch({ type: "ERROR", error });
        //         setError(error);
        //     } finally {
        //         setLoading(false);
        //     }
        // };
        // httpGet();

        // Solution 2:
        // axios.get(URL)
        //     .then(response => {
        //         setLoading(false);
        //         setResponse(response.data.results);
        //     }).catch(err => {
        //         console.error(err);
        //         setLoading(false);
        //         setError(err);
        //     });

        // Solution 3;
        dispatch(fetchData);
    }, [dispatch, fetchData]);


    return [state.response, state.loading, state.error];
};

