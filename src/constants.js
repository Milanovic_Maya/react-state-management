// export const URL = 'https://randomuser.me/api/';
export const URL = `https://my.api.mockaroo.com/users.json?key=${ process.env.REACT_APP_API_KEY }`;

export const initialState = {
    response: null,
    error: null,
    loading: true
};