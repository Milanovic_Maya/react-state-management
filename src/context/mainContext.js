import React, { useReducer, createContext, useCallback, useContext } from "react";
import { CountContext } from "./countContext";
import { MessageContext } from "./messageContext";
import { UsersContext } from "./usersContext";

// create context
export const MainContext = createContext();

// will have consumer and provider

// wrapper around context
const MainContextProvider = ({ children }) => {
    const count = CountContext();
    const message = MessageContext();
    const users = UsersContext();

    const contextValue = {
        count,
        message,
        users
    };
    return (
        <MainContext.Provider value={contextValue}>{children}</MainContext.Provider>
    );
};

export default MainContextProvider;

export const useMainContext = () => useContext(MainContext);

