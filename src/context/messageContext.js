/**
 * 0. import React and/or hooks
 * 1. Define TYPES costants
 * 2. define reducer
 * 3. define context function
 * 4. it has to have export object
 * 5. should have actions to set state
 * 6. export context function
 */



export const MessageContext = () => {
    const message = 'Message here';
    return message;
};
