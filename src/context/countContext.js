import { useCallback, useReducer } from "react";
import state from '../state/state';

const INCR_COUNT = "INCR_COUNT";
const DECR_COUNT = "DECR_COUNT";
const RESET_COUNT = "RESET_COUNT";
const reducer = (state, action) => {
    if (action.type === INCR_COUNT) return state + action.payload;
    if (action.type === DECR_COUNT) return state - action.payload;
    if (action.type === RESET_COUNT) return action.payload;
    return state;
};

export const CountContext = () => {
    const [count, dispatch] = useReducer(reducer, state.initialState);

    //"useCallback" - tell React that this is the same function - does the same thing
    const incrCount = useCallback((e) => {
        dispatch({
            type: INCR_COUNT, payload: 1
        });
    }, [dispatch]);

    const decrCount = useCallback((e) => {
        dispatch({
            type: DECR_COUNT, payload: 1
        });
    }, [dispatch]);

    const resetCount = useCallback((payload) => {
        dispatch({
            type: RESET_COUNT, payload
        });
    }, [dispatch]);


    return {
        count,
        incrCount,
        decrCount,
        resetCount
    };
};

