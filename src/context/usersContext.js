import { useFetch } from "../hooks/customHooks";
import { URL } from './../constants';
import { fetchUsers } from "../actions/customActions";

export const UsersContext = () => {
  const [response, loading, error] = useFetch(URL, fetchUsers);

  return {
    userList: response,
    usersLoading: loading,
    usersError:error
  }
}